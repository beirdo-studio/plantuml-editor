FROM node:14 as builder
WORKDIR /app
ENV VUE_APP_SERVER=http://www.plantuml.com/plantuml

COPY package*.json ./

RUN npm install

COPY . .

RUN npm run flow-typed
RUN npm run build

FROM nginx:stable-alpine


COPY --from=builder /app/dist /usr/share/nginx/html
EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]